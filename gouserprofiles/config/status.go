package config

// Constants for user status
const (
	Pending  = "PENDING"
	GPending = "GPENDING"
	Active   = "ACTIVE"
	GActive  = "GACTIVE"
	Blocked  = "BLOCKED"
)
