package middleware

import (
	"crypto/sha1"
	"encoding/base64"
	"log"

	"golang.org/x/crypto/bcrypt"
)

// HashAndSalt - Hash and Salt the password
func HashAndSalt(plainPwd string) string {
	// Convert to plain password
	pwd := []byte(plainPwd)
	// GenerateFromPassword hashes & salts the password.
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// return the encrypted password
	return string(hash)
}

// ComparePasswords - Compare password for login
func ComparePasswords(dbHashedPwd, plainPwd string) bool {
	// Compare plain password with DB
	dbHashArr := []byte(dbHashedPwd)
	plainArr := []byte(plainPwd)
	err := bcrypt.CompareHashAndPassword(dbHashArr, plainArr)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

// SHAEncrypt - Perform base 64 encoding
func SHAEncrypt(plainPwd string) string {
	h := sha1.New()
	h.Write([]byte(plainPwd))
	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}

// SHAComparePasswords - Compare password for login
func SHAComparePasswords(dbEncryptedPwd, plainPwd string) bool {
	// Compare plain password with DB
	h := sha1.New()
	h.Write([]byte(plainPwd))
	enc := base64.URLEncoding.EncodeToString(h.Sum(nil))
	if enc == dbEncryptedPwd {
		return true
	}
	return false
}
