package middleware

import (
	"gouserprofiles/domain/entity/user"
	"log"
	"net/http"
)

// AuthMiddleWare - Authentication Middleware struct
type AuthMiddleWare struct {
	manager *user.Manager
}

// AssignManager - Assign a manager to auth middleware
func (amw *AuthMiddleWare) AssignManager(m *user.Manager) {
	amw.manager = m
}

// AuthenticateMiddleware - AuthenticateMiddleware uses basic auth to validate user login
func (amw *AuthMiddleWare) AuthenticateMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		newUser := r.Header.Get("NewUser")
		if newUser != "true" {
			email, pwd, _ := r.BasicAuth()
			saved, err := amw.manager.Get(email)
			if err != nil {
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				log.Println("User not found")
				return
			}
			valid := SHAComparePasswords(saved.Password, pwd)
			if valid {
				next.ServeHTTP(w, r)
			} else {
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				log.Println("Request not authenticated")
			}
		} else {
			next.ServeHTTP(w, r)
		}

	})
}
