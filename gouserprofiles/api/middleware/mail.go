package middleware

import (
	"gouserprofiles/config"
	"log"
	"net/smtp"
)

// SendMail - SendMail enables sending an email with smtp via connecting to gmail
func SendMail(to, from, title, body string) error {
	pass := config.MailPassword

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + title + "\n\n" +
		body

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return err
	}

	log.Print("Mail sent, please check your email")
	return nil
}
