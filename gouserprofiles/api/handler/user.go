package handler

import (
	"encoding/base64"
	"encoding/json"
	"gouserprofiles/api/middleware"
	"gouserprofiles/api/presenter"
	"gouserprofiles/config"
	"gouserprofiles/domain/entity/user"
	"log"
	"net/http"
	"strings"
)

// LoginProfileHandler - Login profile handler
func LoginProfileHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("success"))

	})
}

// ViewProfileHandler - View profile handler
func ViewProfileHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}

		errorMsg := "Error reading user"
		var input presenter.User
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		entity := &user.User{
			Email: input.Email,
		}
		if msg, valid := isValidEmail(entity.Email); !valid {
			log.Println(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		saved, err := manager.Get(entity.Email)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		resp := &presenter.User{
			UserID:        saved.UserID,
			Email:         saved.Email,
			Fullname:      saved.Fullname,
			Address:       saved.Address,
			Telephone:     saved.Telephone,
			Status:        saved.Status,
			CreatedDate:   saved.CreatedDate,
			UpdatedDate:   saved.UpdatedDate,
			LastLoginDate: saved.LastLoginDate,
		}

		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
	})
}

// CreateProfileHandler - Create profile handler
func CreateProfileHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, NewUser, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}

		errorMsg := "Error adding user"
		var input presenter.User
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		entity := &user.User{
			Email:    input.Email,
			Status:   config.Pending,
			Password: middleware.SHAEncrypt(input.Password),
		}
		if msg, valid := isValidCreateEntity(entity, manager); !valid {
			log.Println(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		entity.UserID, err = manager.Create(entity)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		resp := &presenter.User{
			UserID:        entity.UserID,
			Email:         entity.Email,
			Status:        entity.Status,
			CreatedDate:   entity.CreatedDate,
			UpdatedDate:   entity.UpdatedDate,
			LastLoginDate: entity.LastLoginDate,
		}

		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
	})
}

// CreateGoogleProfileHandler - Create google profile handler
func CreateGoogleProfileHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, NewUser, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}

		errorMsg := "Error adding user"
		var input presenter.User
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		entity := &user.User{
			Email:    input.Email,
			Fullname: input.Fullname,
			Status:   config.GPending,
			Password: middleware.SHAEncrypt(input.Password),
		}
		if msg, valid := isValidCreateEntity(entity, manager); !valid {
			log.Println(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		entity.UserID, err = manager.Create(entity)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		resp := &presenter.User{
			UserID:        entity.UserID,
			Fullname:      entity.Fullname,
			Email:         entity.Email,
			Status:        entity.Status,
			CreatedDate:   entity.CreatedDate,
			UpdatedDate:   entity.UpdatedDate,
			LastLoginDate: entity.LastLoginDate,
		}

		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
	})
}

// UpdateProfileHandler - Update profile handler
func UpdateProfileHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}

		errorMsg := "Error updating user"
		var input presenter.User
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		// Determine the status of the user
		var status string
		if input.Status != "" {
			status = input.Status
		} else {
			status = config.Active
		}

		entity := &user.User{
			UserID:    input.UserID,
			Email:     input.Email,
			Status:    status,
			Fullname:  input.Fullname,
			Address:   input.Address,
			Telephone: input.Telephone,
			Password:  middleware.SHAEncrypt(input.Password),
		}
		if msg, valid := isValidUpdateEntity(entity, manager); !valid {
			log.Println(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		err = manager.Update(entity)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		resp := &presenter.User{
			UserID:        entity.UserID,
			Email:         entity.Email,
			Fullname:      entity.Fullname,
			Address:       entity.Address,
			Telephone:     entity.Telephone,
			Status:        entity.Status,
			CreatedDate:   entity.CreatedDate,
			UpdatedDate:   entity.UpdatedDate,
			LastLoginDate: entity.LastLoginDate,
		}

		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
	})
}

// ResetPasswordInitiateHandler - Initiate reset password process handler
func ResetPasswordInitiateHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}

		errorMsg := "Error resetting password"
		successMsg := "Password reset link sent"
		var input presenter.User
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		entity := &user.User{
			Email: input.Email,
		}
		if msg, valid := isValidEmail(entity.Email); !valid {
			log.Println(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		saved, err := manager.Get(entity.Email)
		if err != nil || saved.Email != entity.Email {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		saved.ResetToken = middleware.HashAndSalt(entity.Email)
		// Resave with reset token
		err = manager.Update(saved)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		body := "Please visit the following link to reset your password \n \n" +
			config.UIHostAddress + config.TokenParam + saved.ResetToken + ":" + base64.StdEncoding.EncodeToString([]byte(saved.Email))

		err = middleware.SendMail(saved.Email, config.MailFrom, config.MailResetInit, body)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(successMsg))
		return
	})
}

// ResetPasswordHandler - Reset password  handler
func ResetPasswordHandler(manager *user.Manager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			return
		}

		errorMsg := "Error reseting password"
		successMsg := "Password reset successful"
		var input presenter.User
		err := json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		s := strings.Split(input.ResetToken, ":")
		dec, _ := base64.URLEncoding.DecodeString(s[1])
		entity := &user.User{
			Email:      string(dec),
			ResetToken: s[0],
			Password:   input.Password,
		}
		if msg, valid := isValidResetRequest(entity, manager); !valid {
			log.Println(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return
		}
		saved, err := manager.Get(entity.Email)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		// Set new password and save
		saved.Password = middleware.SHAEncrypt(entity.Password)
		saved.ResetToken = ""

		err = manager.Update(saved)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		body := "Your password has been reset successfully \n \n"

		err = middleware.SendMail(saved.Email, config.MailFrom, config.MailResetSuccess, body)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(errorMsg))
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(successMsg))
		return
	})
}
