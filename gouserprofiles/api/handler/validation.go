package handler

import (
	"gouserprofiles/domain/entity/user"
	"regexp"
)

func isValidCreateEntity(user *user.User, manager *user.Manager) (string, bool) {
	if user.Password == "" || user.Email == "" {
		return "Email / Password cannot be empty", false
	}
	if msg, valid := isValidEmail(user.Email); !valid {
		return msg, valid
	}
	existing, _ := manager.Get(user.Email)
	if existing != nil && existing.Email == user.Email {
		return "Email already exists", false
	}
	return "", true
}

func isValidUpdateEntity(user *user.User, manager *user.Manager) (string, bool) {
	if user.Email == "" {
		return "Email cannot be empty", false
	}
	if msg, valid := isValidEmail(user.Email); !valid {
		return msg, valid
	}
	if msg, valid := isValidPhone(user.Telephone); !valid {
		return msg, valid
	}
	existing, _ := manager.Get(user.Email)
	if existing == nil {
		return "User account not found for email", false
	}
	if existing != nil && existing.Email == user.Email && existing.UserID != user.UserID {
		return "Email is associated with another account", false
	}
	return "", true

}

func isValidResetRequest(user *user.User, manager *user.Manager) (string, bool) {
	if user.Email == "" {
		return "Email cannot be empty", false
	}
	if msg, valid := isValidEmail(user.Email); !valid {
		return msg, valid
	}
	existing, _ := manager.Get(user.Email)
	if existing == nil {
		return "User account not found for email", false
	}
	if existing != nil && existing.ResetToken != user.ResetToken {
		return "Reset token is invalid.", false
	}
	return "", true

}

func isValidEmail(email string) (string, bool) {
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(email) {
		return "Please enter a valid email address", false
	}
	return "", true
}

func isValidPhone(phone string) (string, bool) {
	re1 := regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
	if !re1.MatchString(phone) {
		return "Please enter a valid telephone number", false
	}
	return "", true
}
