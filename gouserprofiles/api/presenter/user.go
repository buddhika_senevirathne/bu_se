package presenter

import (
	"time"
)

// User - User json object for working with client request/ response
type User struct {
	UserID        int64     `json:"id,omitempty"`
	Fullname      string    `json:"fullName,omitempty"`
	Email         string    `json:"email,omitempty"`
	Address       string    `json:"address,omitempty"`
	Telephone     string    `json:"telephone,omitempty"`
	CreatedDate   time.Time `json:"createdDate,omitempty"`
	UpdatedDate   time.Time `json:"updatedDate,omitempty"`
	LastLoginDate time.Time `json:"lastLoginDate,omitempty"`
	Status        string    `json:"status,omitempty"`
	Password      string    `json:"password,omitempty"`
	ResetToken    string    `json:"resetToken,omitempty"`
}
