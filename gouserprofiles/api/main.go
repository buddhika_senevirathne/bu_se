package main

import (
	"database/sql"
	"fmt"
	"gouserprofiles/api/handler"
	"gouserprofiles/api/middleware"
	"gouserprofiles/config"
	"gouserprofiles/domain/entity/user"
	"log"
	"net/http"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func main() {
	// Open SQL connection
	dsName := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", config.DBUserName, config.DBPassword, config.DBHost, config.DBPort, config.DBDatabaseName)
	db, err := sql.Open("mysql", dsName)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer db.Close()

	// Initiate sql repo and user manager
	userRepo := user.NewMySQLRepo(db)
	userManager := user.NewManager(userRepo)

	// Initiate Gorilla mux router
	r := mux.NewRouter()
	r.Use(mux.CORSMethodMiddleware(r))

	// Authentication middleware setup
	amw := middleware.AuthMiddleWare{}
	amw.AssignManager(userManager)
	r.Use(amw.AuthenticateMiddleware)

	r.HandleFunc("/login", handler.LoginProfileHandler(userManager)).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/create", handler.CreateProfileHandler(userManager)).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/creategoogle", handler.CreateGoogleProfileHandler(userManager)).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/view", handler.ViewProfileHandler(userManager)).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/update", handler.UpdateProfileHandler(userManager)).Methods(http.MethodPut, http.MethodOptions)
	r.HandleFunc("/resetinit", handler.ResetPasswordInitiateHandler(userManager)).Methods(http.MethodPost, http.MethodOptions)
	r.HandleFunc("/resetpwd", handler.ResetPasswordHandler(userManager)).Methods(http.MethodPut, http.MethodOptions)

	srv := &http.Server{
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
		Addr:         ":" + strconv.Itoa(config.APIPort),
		Handler:      r,
	}
	// Start listen and serve
	log.Println("Server started on : ", config.APIPort)
	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal(err.Error())
	}
}
