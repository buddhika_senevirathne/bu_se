module gouserprofiles

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
)
