package user

import "gouserprofiles/config"

// NewFixtureUser - Returns a fixture user needed for testing
func NewFixtureUser() *User {
	return &User{
		UserID:    1001,
		Fullname:  "Test Full User",
		Email:     "testmail@test.com",
		Address:   "Colombo",
		Telephone: "+94711001001",
		Status:    config.Active,
		Password:  "test@123",
	}
}
