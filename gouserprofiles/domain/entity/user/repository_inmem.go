package user

import (
	"gouserprofiles/domain"
)

// InMemRepo - mySQL repository struct
type InMemRepo struct {
	store map[string]*User
}

// NewInMemRepo - Create new repository
func NewInMemRepo() *InMemRepo {
	var local = map[string]*User{}
	return &InMemRepo{
		store: local,
	}
}

// Create - Create a user
func (repo *InMemRepo) Create(user *User) (int64, error) {
	repo.store[user.Email] = user
	return user.UserID, nil
}

// Get - Get a user by email
func (repo *InMemRepo) Get(email string) (*User, error) {
	if repo.store[email] == nil {
		return nil, domain.ErrorUserNotFound
	}
	return repo.store[email], nil
}

// Update - Update a user via email
func (repo *InMemRepo) Update(user *User) error {
	_, err := repo.Get(user.Email)
	if err != nil {
		return err
	}
	repo.store[user.Email] = user
	return nil
}

// Delete - Delete a user
func (repo *InMemRepo) Delete(email string) error {
	if repo.store[email] == nil {
		return domain.ErrorUserNotFound
	}
	repo.store[email] = nil
	return nil
}
