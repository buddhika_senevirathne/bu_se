package user

// Repository - User respository  interface
type Repository interface {
	Get(email string) (*User, error)
	Create(user *User) (int64, error)
	Update(user *User) error
	Delete(email string) error
}
