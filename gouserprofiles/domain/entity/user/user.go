package user

import (
	"time"
)

// User - Data entity to map user
type User struct {
	UserID        int64
	Fullname      string
	Email         string
	Address       string
	Telephone     string
	CreatedDate   time.Time
	UpdatedDate   time.Time
	LastLoginDate time.Time
	Status        string
	Password      string
	ResetToken    string
}
