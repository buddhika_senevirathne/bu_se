package user

import (
	"gouserprofiles/domain"
	"testing"
)

func Test_Create(t *testing.T) {
	repo := NewInMemRepo()
	m := NewManager(repo)
	u := NewFixtureUser()
	id, err := m.Create(u)
	if err != nil || u.UserID != id {
		t.Error("User creation test fail")
	}

}

func Test_Get(t *testing.T) {
	repo := NewInMemRepo()
	m := NewManager(repo)
	u := NewFixtureUser()
	id, err := m.Create(u)
	if err != nil || u.UserID != id {
		t.Error("User creation fail in get test")
	}
	saved, err := m.Get(u.Email)
	if err != nil || saved.Email != u.Email {
		t.Error("User get test fail")
	}

}
func Test_Update(t *testing.T) {
	repo := NewInMemRepo()
	m := NewManager(repo)
	u := NewFixtureUser()
	_, err := m.Create(u)
	if err != nil {
		t.Error("User creation fail in update test")
	}
	saved, _ := m.Get(u.Email)
	saved.Fullname = "Test Edited Full User"
	err = m.Update(saved)
	if err != nil {
		t.Error("User update fail in update test")
	}
	updated, err := m.Get(u.Email)
	if err != nil || updated.Fullname != "Test Edited Full User" {
		t.Error("User update test fail")
	}
}

func TestDelete(t *testing.T) {
	repo := NewInMemRepo()
	m := NewManager(repo)
	u := NewFixtureUser()
	m.Create(u)

	err := m.Delete(u.Email)
	if err != nil {
		t.Error("User delete test fail")
	}
	err = m.Delete(u.Email)
	if err != domain.ErrorUserNotFound {
		t.Error("User delete exception test fail")
	}
}
