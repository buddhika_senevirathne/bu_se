package user

import (
	"time"
)

// Manager - Manger struct
type Manager struct {
	repo Repository
}

// NewManager -  Create a new user manager
func NewManager(r Repository) *Manager {
	return &Manager{
		repo: r,
	}
}

// Create - Create a user
func (m *Manager) Create(user *User) (int64, error) {
	user.CreatedDate = time.Now()
	return m.repo.Create(user)
}

// Get - Get a user
func (m *Manager) Get(email string) (*User, error) {
	return m.repo.Get(email)
}

// Update - Update a user
func (m *Manager) Update(user *User) error {
	return m.repo.Update(user)
}

// Delete - Delete a user
func (m *Manager) Delete(email string) error {
	return m.repo.Delete(email)
}
