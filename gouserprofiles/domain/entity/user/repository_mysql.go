package user

import (
	"database/sql"
	"log"
	"time"
)

// MySQLRepo - mySQL repository struct
type MySQLRepo struct {
	db *sql.DB
}

// NewMySQLRepo - Create new repository
func NewMySQLRepo(db *sql.DB) *MySQLRepo {
	return &MySQLRepo{
		db: db,
	}
}

// Create - Create a user
func (repo *MySQLRepo) Create(user *User) (int64, error) {
	log.Println("User creation invoked")
	stmt, err := repo.db.Prepare(`
	insert into user (fullname, email, address, telephone, created_date, updated_date, last_login_date, status, password) 
	values (?, ?, ?, ?, ?, ?, ?, ?, ?)`)
	if err != nil {
		return 0, err
	}
	result, err := stmt.Exec(
		user.Fullname,
		user.Email,
		user.Address,
		user.Telephone,
		time.Now().Format("2006-01-02T15:04:05"),
		time.Now().Format("2006-01-02T15:04:05"),
		time.Now().Format("2006-01-02T15:04:05"),
		user.Status,
		user.Password,
	)
	if err != nil {
		return 0, err
	}
	err = stmt.Close()
	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, nil
}

// Get - Get a user by email
func (repo *MySQLRepo) Get(email string) (*User, error) {
	log.Println("User get invoked")
	stmt, err := repo.db.Prepare(`select user_id, fullname, email, address, telephone, created_date, updated_date, last_login_date, status, password, reset_token from user where email = ?`)
	if err != nil {
		return nil, err
	}
	var saved User
	rows, err := stmt.Query(email)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		err = rows.Scan(&saved.UserID, &saved.Fullname, &saved.Email, &saved.Address, &saved.Telephone, &saved.CreatedDate, &saved.UpdatedDate, &saved.LastLoginDate, &saved.Status, &saved.Password, &saved.ResetToken)
	}
	return &saved, nil
}

// Update - Update a user via email
func (repo *MySQLRepo) Update(user *User) error {
	log.Println("User update invoked")
	user.UpdatedDate = time.Now()
	_, err := repo.db.Exec("update user set fullname = ?, email = ?, address = ?, telephone = ?, updated_date = ?, last_login_date = ?, status = ?, password = ?, reset_token = ? where email = ?", user.Fullname, user.Email, user.Address, user.Telephone, user.UpdatedDate.Format("2006-01-02T15:04:05"), user.LastLoginDate.Format("2006-01-02T15:04:05"), user.Status, user.Password, user.ResetToken, user.Email)
	if err != nil {
		return err
	}
	return nil
}

// Delete - Delete a user
func (repo *MySQLRepo) Delete(email string) error {
	_, err := repo.db.Exec("delete from user where email = ?", email)
	if err != nil {
		return err
	}
	return nil
}
