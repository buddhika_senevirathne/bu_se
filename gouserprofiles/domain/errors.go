package domain

import "errors"

// ErrorUserNotFound - User not found error
var ErrorUserNotFound = errors.New("User not found")
