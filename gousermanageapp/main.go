package main

import (
	"gousermanageapp/config"
	"gousermanageapp/handler"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

func main() {

	r := mux.NewRouter()

	r.HandleFunc("/", handler.IndexPageHandler)
	r.HandleFunc("/login", handler.LoginHandler).Methods(http.MethodPost)
	r.HandleFunc("/logout", handler.LogoutHandler).Methods(http.MethodPost)
	r.HandleFunc("/register", handler.RegisterHandler).Methods(http.MethodPost)
	r.HandleFunc("/createaccount", handler.CreateAccountHandler).Methods(http.MethodPost)
	r.HandleFunc("/view", handler.ViewPageHandler).Methods(http.MethodGet)
	r.HandleFunc("/edit", handler.EditPageHandler).Methods(http.MethodPost)
	r.HandleFunc("/update", handler.UpdateHandler).Methods(http.MethodPost)
	r.HandleFunc("/forgot", handler.ForgotHandler).Methods(http.MethodPost)
	r.HandleFunc("/resetinit", handler.ResetInitHandler).Methods(http.MethodPost)
	r.Path("/reset").Queries("token", "{token}").HandlerFunc(handler.ResetPageHandler).Name("ResetPageHandler")
	r.Path("/auth").Queries("code", "{code}", "state", "{state}").HandlerFunc(handler.AuthHandler).Name("AuthHandler")
	r.HandleFunc("/resetpwd", handler.ResetPwdHandler).Methods(http.MethodPost)

	srv := &http.Server{
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
		Addr:         ":" + strconv.Itoa(config.APIPort),
		Handler:      r,
	}
	// Start listen and serve
	log.Println("Server started on : ", config.APIPort)
	err := srv.ListenAndServe()
	if err != nil {
		log.Fatal(err.Error())
	}
}
