package handler

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gousermanageapp/config"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var client = &http.Client{}
var c Credentials
var conf *oauth2.Config

func init() {

	file, err := ioutil.ReadFile("./credentials.json")
	if err != nil {
		fmt.Printf("File error: %v\n", err)
		os.Exit(1)
	}
	json.Unmarshal(file, &c)

	conf = &oauth2.Config{
		ClientID:     c.ClientID,
		ClientSecret: c.Secret,
		RedirectURL:  config.RedirectURL,
		Scopes: []string{
			config.GScopeEmail,
			config.GScopeProfile,
		},
		Endpoint: google.Endpoint,
	}
}

func randomStateToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

func getLoginURL(state string) string {

	return conf.AuthCodeURL(state)

}

// IndexPageHandler - IndexPageHandler
func IndexPageHandler(response http.ResponseWriter, request *http.Request) {
	state := randomStateToken()
	setSessionState(state, response)

	var u User
	u.GoogleLogin = getLoginURL(state)
	tpl, err := template.ParseFiles("templates/index.html")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpl.Execute(response, u)
	if err != nil {
		log.Fatalln(err)
	}
}

// ViewPageHandler - ViewPageHandler
func ViewPageHandler(response http.ResponseWriter, request *http.Request) {
	userName := getUserName(request)
	password := getPassword(request)
	if userName != "" {
		// Make the View call
		var requ User
		requ.Email = userName
		b, err := json.Marshal(requ)
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequest("GET", config.ViewURL, bytes.NewReader(b))
		if err != nil {
			log.Println(err)
			return
		}
		req.SetBasicAuth(userName, password)
		// req.Header.Add("NewUser", "true")
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}
		var u User
		if resp != nil && resp.StatusCode == http.StatusOK {
			u.ErrorMessage = ""
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
			}
			err = json.Unmarshal(body, &u)

			if u.Status == "PENDING" {
				tpl, err := template.ParseFiles("templates/editdetails.html")
				if err != nil {
					log.Fatalln(err)
				}
				err = tpl.Execute(response, u)
				if err != nil {
					log.Fatalln(err)
				}
			} else {
				tpl, err := template.ParseFiles("templates/viewdetails.html")
				if err != nil {
					log.Fatalln(err)
				}
				err = tpl.Execute(response, u)
				if err != nil {
					log.Fatalln(err)
				}
			}
		} else {
			var msg string
			if resp != nil {
				bytesB, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				msg = string(bytesB)
			}
			u.ErrorMessage = "View Failed " + msg
			tpl, err := template.ParseFiles("templates/index.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, u)
			if err != nil {
				log.Fatalln(err)
			}
		}
		defer resp.Body.Close()
	} else {
		http.Redirect(response, request, "/", 302)
	}
}

// UpdateHandler - UpdateHandler
func UpdateHandler(response http.ResponseWriter, request *http.Request) {
	userName := getUserName(request)
	password := getPassword(request)
	if userName != "" {
		// Make the View call
		var requ User
		requ.Email = userName
		requ.Fullname = request.FormValue("fullName")
		requ.Address = request.FormValue("address")
		requ.Telephone = request.FormValue("telephone")
		strid := request.FormValue("id")
		id, _ := strconv.ParseInt(strid, 10, 64)
		requ.UserID = id
		requ.Password = password
		b, err := json.Marshal(requ)
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequest("PUT", config.UpdateURL, bytes.NewReader(b))
		if err != nil {
			log.Println(err)
			return
		}
		req.SetBasicAuth(userName, password)
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}

		if resp != nil && resp.StatusCode == http.StatusOK {
			requ.ErrorMessage = ""
			var u User
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
			}
			err = json.Unmarshal(body, &u)
			tpl, err := template.ParseFiles("templates/viewdetails.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, u)
			if err != nil {
				log.Fatalln(err)
			}
		} else {
			var msg string
			if resp != nil {
				bytesB, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				msg = string(bytesB)
			}
			requ.ErrorMessage = "Update Failed " + msg
			tpl, err := template.ParseFiles("templates/editdetails.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, requ)
			if err != nil {
				log.Fatalln(err)
			}
		}
		defer resp.Body.Close()
	} else {
		http.Redirect(response, request, "/", 302)
	}
}

// CreateAccountHandler - CreateAccountHandler
func CreateAccountHandler(response http.ResponseWriter, request *http.Request) {
	name := request.FormValue("email")
	pass := request.FormValue("password")

	redirectTarget := "/register"
	if name != "" && pass != "" {
		// Make the View call
		var requ User
		requ.Email = name
		requ.Password = pass
		b, err := json.Marshal(requ)
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequest("POST", config.CreateURL, bytes.NewReader(b))
		if err != nil {
			log.Println(err)
			return
		}
		req.Header.Set("NewUser", "true")
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}

		if resp != nil && resp.StatusCode == http.StatusCreated {
			requ.ErrorMessage = ""
			setSession(name, pass, response)
			var u User
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
			}
			err = json.Unmarshal(body, &u)
			tpl, err := template.ParseFiles("templates/editdetails.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, u)
			if err != nil {
				log.Fatalln(err)
			}
		} else {
			var msg string
			if resp != nil {
				bytesB, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				msg = string(bytesB)
			}
			requ.ErrorMessage = "User Creation Failed " + msg
			tpl, err := template.ParseFiles("templates/register.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, requ)
			if err != nil {
				log.Fatalln(err)
			}
		}
		defer resp.Body.Close()
	} else {
		http.Redirect(response, request, redirectTarget, 302)
	}
}

// ResetInitHandler - ResetInitHandler
func ResetInitHandler(response http.ResponseWriter, request *http.Request) {
	name := request.FormValue("email")

	redirectTarget := "/forgot"
	if name != "" {
		// Make the View call
		var requ User
		requ.Email = name
		b, err := json.Marshal(requ)
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequest("POST", config.ResetInitURL, bytes.NewReader(b))
		if err != nil {
			log.Println(err)
			return
		}
		req.Header.Set("NewUser", "true")
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}

		if resp != nil && resp.StatusCode == http.StatusOK {
			requ.ErrorMessage = "Password Link Sent. Please Check Inbox"

			tpl, err := template.ParseFiles("templates/reset.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, requ)
			if err != nil {
				log.Fatalln(err)
			}
		} else {
			var msg string
			if resp != nil {
				bytesB, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				msg = string(bytesB)
			}
			requ.ErrorMessage = "Password Reset Failed " + msg
			tpl, err := template.ParseFiles("templates/reset.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, requ)
			if err != nil {
				log.Fatalln(err)
			}
		}
		defer resp.Body.Close()
	} else {
		http.Redirect(response, request, redirectTarget, 302)
	}
}

// ResetPwdHandler - ResetPwdHandler
func ResetPwdHandler(response http.ResponseWriter, request *http.Request) {
	var requ User
	pass1 := request.FormValue("password")
	pass2 := request.FormValue("password1")
	requ.ResetToken = request.FormValue("token")

	if pass1 != pass2 {
		requ.ErrorMessage = "Passwords do not match"
		tpl, err := template.ParseFiles("templates/resetpwd.html")
		if err != nil {
			log.Fatalln(err)
		}
		err = tpl.Execute(response, requ)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		requ.ErrorMessage = ""
		requ.Password = request.FormValue("password")

		b, err := json.Marshal(requ)
		if err != nil {
			fmt.Println(err)
			return
		}

		req, err := http.NewRequest("PUT", config.ResetPwdURL, bytes.NewReader(b))
		if err != nil {
			log.Println(err)
			return
		}
		req.Header.Set("NewUser", "true")
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		}

		if resp != nil && resp.StatusCode == http.StatusOK {
			requ.ErrorMessage = "Password Reset Succesful"
			requ.Email = ""
			requ.Password = ""

			tpl, err := template.ParseFiles("templates/index.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, requ)
			if err != nil {
				log.Fatalln(err)
			}
		} else {
			var msg string
			if resp != nil {
				bytesB, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				msg = string(bytesB)
			}
			requ.ErrorMessage = "Password Reset Failed " + msg
			requ.Password = ""
			tpl, err := template.ParseFiles("templates/resetpwd.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, requ)
			if err != nil {
				log.Fatalln(err)
			}
		}
		defer resp.Body.Close()

	}
}

// LoginHandler - LoginHandler
func LoginHandler(response http.ResponseWriter, request *http.Request) {
	name := request.FormValue("email")
	pass := request.FormValue("password")

	redirectTarget := "/"
	if name != "" && pass != "" {

		req, err := http.NewRequest("POST", config.LoginURL, nil)
		if err != nil {
			log.Println(err)
			return
		}
		req.SetBasicAuth(name, pass)
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
			return
		}
		var u User
		if resp != nil && resp.StatusCode == http.StatusOK {
			u.ErrorMessage = ""
			setSession(name, pass, response)
			redirectTarget = "/view"
		} else {
			var msg string
			if resp != nil {
				bytesB, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				msg = string(bytesB)
			}
			u.ErrorMessage = "Login Failed " + msg
			tpl, err := template.ParseFiles("templates/index.html")
			if err != nil {
				log.Fatalln(err)
			}
			err = tpl.Execute(response, u)
			if err != nil {
				log.Fatalln(err)
			}
		}
		defer resp.Body.Close()
	}
	http.Redirect(response, request, redirectTarget, 302)
}

// EditPageHandler - EditPageHandler
func EditPageHandler(response http.ResponseWriter, request *http.Request) {
	userName := getUserName(request)
	password := getPassword(request)

	redirectTarget := "/"
	if userName != "" && password != "" {
		var requ User
		requ.Email = userName
		requ.Fullname = request.FormValue("fullName")
		requ.Address = request.FormValue("address")
		requ.Telephone = request.FormValue("telephone")
		strid := request.FormValue("id")
		id, _ := strconv.ParseInt(strid, 10, 64)
		requ.UserID = id
		requ.Password = password

		tpl, err := template.ParseFiles("templates/editdetails.html")
		if err != nil {
			log.Fatalln(err)
		}
		err = tpl.Execute(response, requ)
		if err != nil {
			log.Fatalln(err)
		}
	}
	http.Redirect(response, request, redirectTarget, 302)
}

// RegisterHandler - RegisterHandler
func RegisterHandler(response http.ResponseWriter, request *http.Request) {
	var requ User

	tpl, err := template.ParseFiles("templates/register.html")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpl.Execute(response, requ)
	if err != nil {
		log.Fatalln(err)
	}
}

// ResetPageHandler - ResetPageHandler
func ResetPageHandler(response http.ResponseWriter, request *http.Request) {
	var requ User

	token := request.FormValue("token")

	requ.ResetToken = token

	tpl, err := template.ParseFiles("templates/resetpwd.html")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpl.Execute(response, requ)
	if err != nil {
		log.Fatalln(err)
	}
}

// AuthHandler - AuthHandler
func AuthHandler(response http.ResponseWriter, request *http.Request) {

	var u User
	code := request.FormValue("code")
	state := request.FormValue("state")

	savedState := getState(request)

	if savedState != state {
		u.ErrorMessage = "Authorization Failed "
		tpl, err := template.ParseFiles("templates/index.html")
		if err != nil {
			log.Fatalln(err)
		}
		err = tpl.Execute(response, u)
		if err != nil {
			log.Fatalln(err)
		}
		return
	}

	//  Exchange code used initiate a transport.
	token, err := conf.Exchange(oauth2.NoContext, code)
	if err != nil {
		u.ErrorMessage = "Exchange Creation Failed "
		tpl, err := template.ParseFiles("templates/index.html")
		if err != nil {
			log.Fatalln(err)
		}
		err = tpl.Execute(response, u)
		if err != nil {
			log.Fatalln(err)
		}
		return
	}

	// Construct the client using received code
	client := conf.Client(oauth2.NoContext, token)
	resp, err := client.Get(config.GUserInfoURL)
	if err != nil {
		u.ErrorMessage = "Client Call Failed "
		tpl, err := template.ParseFiles("templates/index.html")
		if err != nil {
			log.Fatalln(err)
		}
		err = tpl.Execute(response, u)
		if err != nil {
			log.Fatalln(err)
		}
		return
	}
	defer resp.Body.Close()
	data, _ := ioutil.ReadAll(resp.Body)

	var gu GoogleUser
	json.Unmarshal(data, &gu)
	if gu.Email != "" {
		setSession(gu.Email, gu.Email, response)
	}
	log.Println(gu.Email)

	// Get exisiting users if there are by email
	var requ User
	requ.Email = gu.Email
	b, err := json.Marshal(requ)
	if err != nil {
		fmt.Println(err)
		return
	}

	req, err := http.NewRequest("GET", config.ViewURL, bytes.NewReader(b))
	if err != nil {
		log.Println(err)
		return
	}
	req.Header.Set("NewUser", "true")
	resp, err = client.Do(req)
	if err != nil {
		log.Println(err)
	}
	// If user found show editable or view based on status
	var saved User
	if resp != nil && resp.StatusCode == http.StatusOK {
		saved.ErrorMessage = ""
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}
		err = json.Unmarshal(body, &saved)

		if saved.Email != "" {
			if saved.Status == "GPENDING" {
				//Set status to reflect google user
				saved.Status = "GACTIVE"
				tpl, err := template.ParseFiles("templates/editdetails.html")
				if err != nil {
					log.Fatalln(err)
				}
				err = tpl.Execute(response, saved)
				if err != nil {
					log.Fatalln(err)
				}
			} else {
				tpl, err := template.ParseFiles("templates/viewdetails.html")
				if err != nil {
					log.Fatalln(err)
				}
				err = tpl.Execute(response, saved)
				if err != nil {
					log.Fatalln(err)
				}
			}
		} else {
			// If user not found create a user
			var requ User
			requ.Email = gu.Email
			requ.Password = gu.Email
			requ.Fullname = gu.GivenName + " " + gu.FamilyName
			b, err := json.Marshal(requ)
			if err != nil {
				fmt.Println(err)
				return
			}

			req, err := http.NewRequest("POST", config.CreateGoogleURL, bytes.NewReader(b))
			if err != nil {
				log.Println(err)
				return
			}
			req.Header.Set("NewUser", "true")
			resp, err := client.Do(req)
			if err != nil {
				log.Println(err)
			}

			if resp != nil && resp.StatusCode == http.StatusCreated {
				requ.ErrorMessage = ""
				setSession(gu.Email, gu.Email, response)
				var u User
				body, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
				}
				err = json.Unmarshal(body, &u)
				//Set status to reflect google user
				u.Status = "GACTIVE"
				tpl, err := template.ParseFiles("templates/editdetails.html")
				if err != nil {
					log.Fatalln(err)
				}
				err = tpl.Execute(response, u)
				if err != nil {
					log.Fatalln(err)
				}
			}

		}
	}
	defer resp.Body.Close()
}

// ForgotHandler - ForgotHandler
func ForgotHandler(response http.ResponseWriter, request *http.Request) {
	var requ User

	tpl, err := template.ParseFiles("templates/reset.html")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpl.Execute(response, requ)
	if err != nil {
		log.Fatalln(err)
	}
}

// LogoutHandler - LogoutHandler
func LogoutHandler(response http.ResponseWriter, request *http.Request) {
	clearSession(response)
	http.Redirect(response, request, "/", 302)
}
