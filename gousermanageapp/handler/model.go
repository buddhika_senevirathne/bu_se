package handler

import "time"

// User - User json object for working with client request/ response
type User struct {
	UserID        int64     `json:"id,omitempty"`
	Fullname      string    `json:"fullName,omitempty"`
	Email         string    `json:"email,omitempty"`
	Address       string    `json:"address,omitempty"`
	Telephone     string    `json:"telephone,omitempty"`
	CreatedDate   time.Time `json:"createdDate,omitempty"`
	UpdatedDate   time.Time `json:"updatedDate,omitempty"`
	LastLoginDate time.Time `json:"lastLoginDate,omitempty"`
	Status        string    `json:"status,omitempty"`
	Password      string    `json:"password,omitempty"`
	ResetToken    string    `json:"resetToken,omitempty"`
	ErrorMessage  string    `json:"errorMessage,omitempty"`
	GoogleLogin   string    `json:"googleLogin,omitempty"`
}

// Credentials -  Googles ID and secret
type Credentials struct {
	ClientID string `json:"clientId"`
	Secret   string `json:"secret"`
}

// GoogleUser - GoogleUser struct to parse response
type GoogleUser struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Profile       string `json:"profile"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified string `json:"email_verified"`
	Gender        string `json:"gender"`
}
