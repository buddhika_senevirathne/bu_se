package config

// Constants for DB access and API exposing
const (
	APIPort         = 8085
	ViewURL         = "http://localhost:8080/view"
	LoginURL        = "http://localhost:8080/login"
	UpdateURL       = "http://localhost:8080/update"
	CreateURL       = "http://localhost:8080/create"
	CreateGoogleURL = "http://localhost:8080/creategoogle"
	ResetInitURL    = "http://localhost:8080/resetinit"
	ResetPwdURL     = "http://localhost:8080/resetpwd"
	RedirectURL     = "http://murthi.co/auth"
	GScopeEmail     = "https://www.googleapis.com/auth/userinfo.email"
	GScopeProfile   = "https://www.googleapis.com/auth/userinfo.profile"
	GScopeOpenID    = "openid"
	GUserInfoURL    = "https://www.googleapis.com/oauth2/v3/userinfo"
)
